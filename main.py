#!/usr/bin/python
# -*- coding: utf-8 -*-

import pprint
import time
from datetime import datetime, timedelta
from urllib.parse import parse_qs


def parse_raw_data(raw_data_filename, interval_in_min=5):
    """
    Parse log file with raw data and reduce data to intervals
    :param interval_in_min: Interval in minutes
    :param raw_data_filename: Log filename
    :return: Dict in format: {'%app%_%country%_%interval%': %installs_count%}
    """
    install_time_format = '%Y-%m-%d %H:%M:%S.%f'
    res = {}
    with open(raw_data_filename, 'r', encoding='utf-8') as csv_file:
        content = csv_file.readlines()
        for line in content:
            query_string = line.split('; ')[7].strip()
            query_params = parse_qs(query_string, keep_blank_values=True, encoding='utf-8')

            app_id = query_params['app_id'][0]
            install_time = query_params['install_time'][0]
            install_datetime = datetime.strptime(install_time, install_time_format)
            country_code = query_params['country_code'][0]
            interval_start_datetime = install_datetime - timedelta(
                minutes=install_datetime.minute % interval_in_min,
                seconds=install_datetime.second,
                microseconds=install_datetime.microsecond
            )
            interval_start_timestamp = time.mktime(interval_start_datetime.timetuple())

            key = '{}_{}_{}'.format(app_id, country_code, interval_start_timestamp)
            try:
                res[key] += 1
            except KeyError:
                res[key] = 0
    return res


def print_statistics(installs):
    """
    Output statistics
    :param installs: Dict in format: {'%app%_%country%_%interval%': %installs_count%}
    :return:
    """
    for key, installs_count in installs.items():
        splitted_key = key.split('_')
        app_id = splitted_key[0]
        country_code = splitted_key[1]
        interval_start_timestamp = splitted_key[2]
        res = ' %-25s %-5s %-15s %-5s' % (app_id, country_code, interval_start_timestamp, installs_count)
        print(res)


parsed_installs = parse_raw_data('sample_installs.csv')
# Just for debug purposes:
# pp = pprint.PrettyPrinter(depth=6)
# pp.pprint(parsed_installs)
print_statistics(parsed_installs)
